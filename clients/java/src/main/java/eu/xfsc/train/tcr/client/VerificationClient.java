package eu.xfsc.train.tcr.client;

import java.util.Map;

import org.springframework.web.reactive.function.client.WebClient;

import eu.xfsc.train.tcr.api.generated.model.VerificationRequest;
import eu.xfsc.train.tcr.api.generated.model.VerificationResult;

public class VerificationClient extends ServiceClient {

    public VerificationClient(String baseUrl, String jwt) {
        super(baseUrl, jwt);
    }

    public VerificationClient(String baseUrl, WebClient client) {
        super(baseUrl, client);
    }
    
    public VerificationResult resolveIssuer(String issuer, String pointer) {
    	VerificationRequest vrq = new VerificationRequest();
    	vrq = vrq.issuer(issuer).trustSchemePointer(pointer);
        return doPost(baseUrl + "/resolve", vrq, Map.of(), VerificationResult.class);
    }

}
