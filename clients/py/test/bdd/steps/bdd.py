from dataclasses import dataclass
from behave import *

import requests

SESSION = requests.Session()


@dataclass
class TrustedContentResolver:
    app: str


@given('we have XFSC TRAIN Trusted Content Resolver REST API')
def step_impl(context):
    # TODO make a heartbeat requests
    # ask kubernetis url
    # load trusted-content-resolver/service/src/main/resources/application.yml into context
    context.ctr = TrustedContentResolver('http://localhost:8087')
    pass


@given('an actor X to be verified')
def step_impl(context):
    # TODO check the data if available
    # load input data for actor x into context
    context.actor_x = {
        'some': 'info'
    }


@when('we initiate a verification actor X')
def step_impl(context):
    response = SESSION.get(f'https://example.com')

    #response = SESSION.post(f'{context.ctr.app}/tcr/verify', data=context.actor_x['some'])
    context.ctr.response = response


@then('actor X gets verified')
def step_impl(context):
    assert context.ctr.response


@when("we initiate a verification actor Y")
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    raise NotImplementedError(u'STEP: When we initiate a verification actor Y')


# @given(
#     "A request update of trust frameworks and DID configuration is successfully reflected in the DNS Zone File (200)")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     # GET to DNS which return success DID configuration
#     raise NotImplementedError(
#         u'STEP: Given A request update of trust frameworks and DID configuration is successfully reflected in the DNS Zone File (200)')
#
#
# @step(
#     "An instantiation of a trust list is reflected in the trust list storage with possibility to retrieve via API endpoints")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     # qhuery database for
#     raise NotImplementedError(
#         u'STEP: And An instantiation of a trust list is reflected in the trust list storage with possibility to retrieve via API endpoints')
#
#
# @given("trust framework pointers example.federation1.de and example.federation2.de")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     context.trust_framework_pointers = 'example.federation1.de' and 'example.federation2.de']
#
#
#     @when("Navigate to listed trust framework pointers")
#     def step_impl(context):
#         """
#         :type context: behave.runner.Context
#         """
#         data = VerificationRequest(context.trust_framework_pointers)
#         response = SESSION.post(f'{context.ctr.app}/tcr/verify', data=data.json())
#         context.ctr.response = response
#
#
# @then("have example.federation1.de and example.federation2.de should be in trust List VC endpoint")
# def step_impl(context):
#     """
#     :type context: behave.runner.Context
#     """
#     assert sql.query('SELECT WHERE x in [xample.federation1.de and example.federation2.de]') is True
