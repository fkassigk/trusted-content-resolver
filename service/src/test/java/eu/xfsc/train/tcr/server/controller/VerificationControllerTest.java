package eu.xfsc.train.tcr.server.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.xfsc.train.tcr.api.generated.model.VerificationRequest;
import eu.xfsc.train.tcr.api.generated.model.VerificationResult;


@SpringBootTest
@AutoConfigureMockMvc
//@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class VerificationControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper jsonMapper;

    
    @Test
    public void postVerifyRequestShouldReturnSuccessResponse() throws Exception {
      
        VerificationRequest request = new VerificationRequest();
        request.setIssuer("https://test-issuer.sample.org");
        request.setTrustSchemePointer("did-web.test.train.trust-scheme.de"); 
        
        String response = mockMvc
            .perform(MockMvcRequestBuilders.post("/resolve")
            .content(jsonMapper.writeValueAsString(request))		
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();
        VerificationResult result = jsonMapper.readValue(response, VerificationResult.class);
        assertNotNull(result);
        assertTrue(result.getTrustListEndpoints().contains("did:web:essif.trust-scheme.de"));
    }
    
}
