package eu.xfsc.train.tcr.server.legacy;

public class DANEException extends Exception {
    
    public DANEException(String s) {
        super(s);
    }
    
    public DANEException(Exception e) {
        super(e);
    }
}

