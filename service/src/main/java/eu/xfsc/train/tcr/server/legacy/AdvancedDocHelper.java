package eu.xfsc.train.tcr.server.legacy;

import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;


public abstract class AdvancedDocHelper {
    
    private Map<X509Certificate, Boolean> verificationResults;
    
    public AdvancedDocHelper() {
        verificationResults = new HashMap<>();
    }
    
    public boolean verify(X509Certificate forCert) {
        //SubjectKeyIdentifier subjectKeyIdentifier = X509Helper.genSubjectKeyIdentifier(forCert);
        if (verificationResults.containsKey(forCert)) {
            return this.verificationResults.get(forCert);
        }
    
        //AdvancedDocHelper.logger.error("No verification result for cert " + forCert.getIssuerDN());
        return false;
    }
    
    protected void setVerificationResult(X509Certificate certificate, boolean result) {
        //SubjectKeyIdentifier subjectKeyIdentifier = X509Helper.genSubjectKeyIdentifier(certificate);
        verificationResults.put(certificate, result);
    }
    
    public abstract boolean verify();
    
    public abstract boolean verify(boolean skipSignatureValidation);
    
    public abstract X509Certificate getCertificate();
}
