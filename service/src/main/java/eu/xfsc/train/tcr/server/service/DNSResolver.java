package eu.xfsc.train.tcr.server.service;

import java.io.IOException;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jitsi.dnssec.validator.ValidatingResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.xbill.DNS.DClass;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.PTRRecord;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.Type;
import org.xbill.DNS.URIRecord;

import eu.xfsc.train.tcr.server.exception.DNSException;
import eu.xfsc.train.tcr.server.legacy.TrustScheme;
import eu.xfsc.train.tcr.server.legacy.TrustSchemeClaim;
import eu.xfsc.train.tcr.server.legacy.TrustSchemeFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DNSResolver {
	

	private Resolver resolver;
	
	public DNSResolver(@Value("${tcr.dns.hosts}") List<String> dnsHosts, @Value("${tcr.dns.timeout}") int timeout) {
		log.info("<init>.enter; configured DNS hosts: {}, timeout: {}", dnsHosts, timeout);
		try {
			if (dnsHosts.isEmpty()) {
			    resolver = new SimpleResolver();
			} else {
	            SimpleResolver[] resolvers = new SimpleResolver[dnsHosts.size()];
	            int idx = 0;
	            for (String dnsHost: dnsHosts) {
	            	SimpleResolver r = new SimpleResolver(dnsHost);
	            	if (timeout > 0) {
	            	    r.setTimeout(Duration.ofMillis(timeout));
	            	}
	            	resolvers[idx] = r;
	            	idx++;
	            }
	            resolver = new ExtendedResolver(resolvers);
			}
			if (timeout > 0 ) {
	            resolver.setTimeout(Duration.ofMillis(timeout));
			}
		} catch (UnknownHostException ex) {
			log.error("<init}.error", ex);
			throw new IllegalArgumentException(ex);
		}
		log.info("<init>.exit; configured resolver: {}", resolver);
	}
	
	public Collection<String> resolveDid(String domain) throws Exception {
		Set<String> results = new HashSet<>();
	    Set<String> uris = resolvePtr(domain);
	    for (String uri: uris) {
	        Set<String> dids = resolveUri(uri);
	        results.addAll(dids);
	    }
		return results;
	}
	
	private static final String PREFIX = "_scheme._trust.";
	
    private String fixHostName(String host) {
        if (host.startsWith(PREFIX)) {
            return host;
        } else {
            return PREFIX + host;
        }
    }
    
    private Set<String> resolvePtr(String ptr) {
        log.debug("resolvePtr.enter; got PTR: {}", ptr);
        String host = fixHostName(ptr);
        
        Set<String> set;
        try {
            set = query(host, Type.PTR)
            	.filter(r -> r instanceof PTRRecord)
                .map(r -> ((PTRRecord) r).getTarget().toString())
                .collect(Collectors.toSet());
        } catch (IOException | DNSException e) {
            log.warn("resolvePtr.error", e);
            set = Collections.emptySet();
        }
        log.debug("resolvePtr.exit; returning uris: {}", set);
        return set;
    }
    
    private Set<String> resolveUri(String uri) {
        log.debug("resolveUri.enter; got URI: {}", uri);
        String host = fixHostName(uri);
        
        Set<String> set;
        try {
            set = query(host, Type.URI)
            	.filter(r -> r instanceof URIRecord)
                .map(r -> ((URIRecord) r).getTarget())
                .collect(Collectors.toSet());
        } catch (IOException | DNSException e) {
            log.warn("resolveUri.error", e);
            set = Collections.emptySet();
        }
        log.debug("resolveUri.exit; returning dids: {}", set);
        return set;
    }
	
	
    private Stream<Record> query(String host, int type) throws IOException, DNSException {
        if (!host.endsWith(".")) {
            host = host + ".";
        }
        
        Record query = Record.newRecord(Name.fromConstantString(host), type, DClass.IN);
        log.debug("query; DNS query: " + query.toString());
        Message response = resolver.send(Message.newQuery(query));
        //if (!response.getHeader().getFlag(Flags.AD)) {
        //    log.debug("query; NO AD flag present!");
            //throw new DNSException("No AD flag. (Host not using DNSSec?)");
        //}
        
        int rcode = response.getRcode();
        if (rcode != Rcode.SERVFAIL && rcode != Rcode.NOERROR) {
            for (RRset set: response.getSectionRRsets(Section.ADDITIONAL)) {
                log.debug("query; Zone: {}", set.getName());
                if (set.getName().equals(Name.root) && set.getType() == Type.TXT && set.getDClass() == ValidatingResolver.VALIDATION_REASON_QCLASS) {
                    log.info("query; Reason: {}", ((TXTRecord) set.first()).getStrings().get(0));
                }
                throw new DNSException("RCode: " + rcode + " (" + Rcode.string(rcode) + ")");
            }
        }
        
        List<RRset> rrSets = response.getSectionRRsets(Section.ANSWER);
        return rrSets.stream().flatMap(s -> s.rrs().stream());
    }
    
	
    
	
    public Collection<String> verifyIdentity(String issuer, String claim) throws Exception {
        log.debug("verifyIdentity.enter; issuer: {}, claim: {}", issuer, claim);

        TrustSchemeClaim tsClaim = new TrustSchemeClaim(claim);
        TrustSchemeFactory tsFactory = new TrustSchemeFactory();

        List<String> resp = null;
        Collection<TrustScheme> schemes = tsFactory.createTrustSchemes(tsClaim);
        if (schemes.isEmpty()) {
            throw new IOException("Did not find TrustScheme / TrustList");
        }

        //resp.VerificationResult.FoundCorrespondingTrustScheme = scheme.getSchemeIdentifierCleaned();
        //resp.VerificationResult.TrustListDiscoveryInitiated = true;
        //resp.VerificationResult.TrustListFoundAndLoaded = scheme.getTSLlocation();

        //XMLUtil util = new XMLUtil(scheme.getTSLcontent());
        //String DID = util.getElementByXPath("//TrustServiceProviderList/TrustServiceProvider/TSPInformation/DIDName/Name[1]/text()");
        //report.addLine("DID (extracted): " + DID);
            
        resp = schemes.stream().map(s -> s.getTSLcontent()).toList();

        //if (DID.equals(issuer)) {
        //    resp.VerificationResult.FoundIssuer = issuer;
        //    resp.VerificationResult.VerifyIssuer = true;
        //    resp.VerificationResult.VerificationSuccessful = true;
        //    resp.VerificationStatus = true;
        //}
        return resp;
    }	

}
