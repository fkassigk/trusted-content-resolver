package eu.xfsc.train.tcr.server.exception;

public class DNSException extends Exception {
    
    public DNSException(String s) {
        super(s);
    }
}

