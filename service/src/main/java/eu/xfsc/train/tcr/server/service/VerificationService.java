package eu.xfsc.train.tcr.server.service;


import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import eu.xfsc.train.tcr.api.generated.model.VerificationRequest;
import eu.xfsc.train.tcr.api.generated.model.VerificationResult;
import eu.xfsc.train.tcr.server.generated.controller.TrustedContentResolverApiDelegate;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation of the {@link eu.xfsc.fc.server.generated.controller.SessionApiDelegate} interface.
 */
@Slf4j
@Service
public class VerificationService implements TrustedContentResolverApiDelegate {
	
	@Autowired
	private DNSResolver dns;


    @Override
    public ResponseEntity<VerificationResult> resolveIssuer(VerificationRequest verificationRequest) {
        log.debug("resolveIssuer.enter; got request: {}", verificationRequest);
        try {
			Collection<String> addresses = dns.resolveDid(verificationRequest.getTrustSchemePointer());
        	//Collection<String> addresses = dns.verifyIdentity(verificationRequest.getIssuer(), verificationRequest.getTrustSchemePointer());
	        VerificationResult result = new VerificationResult();
	        result.setResolvedDID(verificationRequest.getIssuer());
	        result.setTrustListEndpoints(new ArrayList<>(addresses));
	        log.debug("resolveIssuer.exit; returning result: {}", result);
	        return ResponseEntity.ok(result);
		} catch (Exception ex) {
			log.error("resolveIssuer.error", ex);
			ResponseEntity response = ResponseEntity.internalServerError().body(ex.getMessage());
			return response;
		}
    }

}
