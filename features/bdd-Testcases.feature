[IDM.TRAIN.00016] Trusted Content Resolver (Trust Discovery)

Scenario:	00016-A3_Navigate multiple trust framework pointers

Given:	    The TCR is running
            The DNS Resolver is running
            The DNS entry is configured
            
And:		Multiple Trust Framework Pointers exist
		    DNS SEC is correct

When:		Multiple Trust Framework Pointers are supplied in discovered trust request

And:		Issuer details is “did:example:123456789abcdefghijk”

Then:		Navigate multiple trust framework pointers

And:		Check all of them
		    Check return of Trust List
